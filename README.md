# LineageOS performance profile quick tile

This app adds a quick tile for changing the current lineageos performance profile, because apparently that's not a thing by default (as of Lineageos 15.1, Jan 2020)
For this to actually work you need to install it as a privileged system app. On magisk you can use the terminal app systemizer for that.

## License
This app is licensed under the GNU GPLv3.

ic_perf_profile*.xml files are taken from here https://github.com/LineageOS/android_packages_apps_LineageParts (branch lineage-15.1)
Not sure if I need to include this but just to be safe, the copyright notice from that project is as follows:
Copyright (C) 2016 The CyanogenMod Project
              2017-2019 The LineageOS Project
